# Métodos destructivos

En general, los métodos que terminan en `!`, indican que el método modificará el objeto en el que se llama. Ruby llama a estos `"dangerous methods"`.

Ejecuta el siguiente programa :

```ruby
first_name = "Carlos"
first_name.upcase

p first_name
# Podemos observar que al imprimir la variable first_name, se encuentra en su estado inicial.
#=>"Carlos"
# Ya que aun usando el metodo upcase los cambios no se guardan al ser llamado de nuevo.


# Aplicando el método destructivo

last_name = "Rangel"
last_name.upcase!

p last_name
# Al utilizar el exclamation mark (!), los cambios al utilizar 'upcase' son  
# permanentementes al ser llamada la variable.
#=>"RANGEL"
```

## Ejercicio - Nombre Correcto

Corrige el siguiente código para que valide el mensaje `"nombre correcto"`. Es importante usar solamente métodos destructivos. Las pruebas `test1` y `test2` deben ser true. 

```ruby
first_name = "Rogelio"
last_name = "manzano"
mensaje = "Nombre Correcto"

if first_name == "ROGELIO" && last_name == "Manzano"
  if mensaje == "nombre correcto"
    test1 = first_name == "ROGELIO"
    test2 = last_name == "Manzano"
  end
end

#driver code

p test1 == true
p test2 == true
```
